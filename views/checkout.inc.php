<?php
try {
if (!isset($_SESSION['member']) || !is_a($_SESSION['member'],"Member"))
{
    header("Location: " . Router::getSourcePath() . "index.php");

}

    require_once Router::getSourcePath()."inc/helper_func.inc.php";

// เก็บข้อมูลจากสิ่งที่ controller เตรียมไว้ให้
    $products = $_SESSION['productList'];

// เริ่มต้นการเขียน view
    $title = "Check Out";
    ob_start();
    ?>

    <h1>ยินดีตอนรับสู่ N.S. Shop</h1>
        <?php
        $header = array("ลำดับ","ชื่อสินค้า","ราคาต่อชิ้น (บาท)","จำนวน","ราคารวม (บาท)");
        $data = array();
        $i = 0;
        $price = [];
        $number = [];
        $subTotal = [];
        $priceNoVat = 0;
        $priceWithVat = 0;
        foreach ($products as $prod) {
            if ($_POST["{$prod->getProductId()}"] > 0) {
                $price[$i] = $prod->getPrice();
                $number[$i] = $_POST["{$prod->getProductId()}"];
                $i++;
                }
        }
        $i = 0;
        calculateTotalPrice($price,$number,$subTotal, $priceNoVat, $priceWithVat);
        foreach ($products as $prod) {
            if($_POST["{$prod->getProductId()}"]>0) {
                $data[$i] = array($i + 1, $prod->getProductName(), $prod->getPrice(), $_POST["{$prod->getProductId()}"],$subTotal[$i]);
                $i++;
            }
        }
        showTable($header,$data);
        echo "<table width=\"60%\" style=\"text-align: center; border: 0px solid black; margin:auto\">
            <tr align=\"center\">
        <td colspan='4'> ราคารวมทั้งหมด (excl.Vat)<br/>
                        VAT(7%)<br/>
                        ราคารวมทั้งหมด (incl.Vat)</td>
        <td>";
            echo number_format($priceNoVat,2);echo"<br/>";
            echo number_format($priceWithVat-$priceNoVat,2);echo"<br/>";
            echo number_format($priceWithVat,2);
            echo "</td></tr>
        </table>";
        ?>
        <div style="margin: 1em; padding: 2em">
            <a href=<?= Router::getSourcePath() . "index.php?controller=Product&action=cart"?>>Back</a>
        </div>

    <?php
    $content = ob_get_clean();

    include Router::getSourcePath()."templates/layout.php";
} catch (Throwable $e) { // PHP 7++
    echo "Access denied: No Permission to view this page";
    exit(1);
}
?>
