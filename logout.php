<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 18/3/2562
 * Time: 22:58
 */
session_start();
unset($_SESSION['member']);
unset($_SESSION['productList']);
session_destroy();
header("Location: index.php");