<?php
/**
 * Created by PhpStorm.
 * User: PC-8303-01
 * Date: 28/2/2562
 * Time: 11:02
 */

class Member
{
    private $id;
    private $username;
    private $passwd;
    private $name;
    private $surname;

    public static function findByAccount($username,$password): ?Member
    {
        $con = Db::getInstance();
        $query = "SELECT * FROM members WHERE username = '$username' and passwd = '$password'";
        $stmt = $con->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_CLASS, "Member");
        $stmt->execute();
        if ($mem = $stmt->fetch())
        {
            return $mem;
        }
        return null;
    }
    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPasswd(): string
    {
        return $this->passwd;
    }

    /**
     * @param mixed $passwd
     */
    public function setPasswd(string $passwd): void
    {
        $this->passwd = $passwd;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getIterator()
    {
        return new ArrayIterator(get_object_vars($this));
    }
}